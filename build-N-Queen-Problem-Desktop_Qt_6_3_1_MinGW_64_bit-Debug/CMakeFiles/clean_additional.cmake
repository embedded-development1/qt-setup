# Additional clean files
cmake_minimum_required(VERSION 3.16)

if("${CONFIG}" STREQUAL "" OR "${CONFIG}" STREQUAL "Debug")
  file(REMOVE_RECURSE
  "CMakeFiles\\N-Queen-Problem_Lib_autogen.dir\\AutogenUsed.txt"
  "CMakeFiles\\N-Queen-Problem_Lib_autogen.dir\\ParseCache.txt"
  "CMakeFiles\\N-Queen-Problem_autogen.dir\\AutogenUsed.txt"
  "CMakeFiles\\N-Queen-Problem_autogen.dir\\ParseCache.txt"
  "N-Queen-Problem_Lib_autogen"
  "N-Queen-Problem_autogen"
  )
endif()
