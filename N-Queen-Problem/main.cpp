#include <QCoreApplication>
#include <iostream>
#include <limits>
#include "queen.h"
using namespace std;

int row;
int column;
int boardSize = 0;
int numOfQueens = 0;
int firstPlacement = 0;
bool repeat = true;

void start(){
    while(repeat){
        qInfo()<<"The N queen problem.";
        while(numOfQueens < 1){
            qInfo()<<"How many queens? [1-10]";

            char c;
            if(!(cin >> numOfQueens) || (cin.get(c) && !isspace(c))){
                qInfo()<<"Entry must be a number! Try again: ";
                cin.clear();
                cin.ignore(500, '\n');
                numOfQueens = 0;
            }
            else if(numOfQueens < 1 || numOfQueens > 10){
                qInfo()<<"Number must be in Range! Try again: ";
                cin.clear();
                cin.ignore(500, '\n');
                numOfQueens = 0;
            }
        }
        while(boardSize < 1){
            qInfo()<<"How big is the board?[NxN][1-10]";

            char c;
            if(!(cin >> boardSize) || (cin.get(c) && !isspace(c))){
                qInfo()<<"Entry must be a number! Try again: ";
                cin.clear();
                cin.ignore(500, '\n');
                boardSize = 0;
            }
            else if(boardSize < 1 || boardSize > 10){
                qInfo()<<"Number must be in Range! Try again: ";
                cin.clear();
                cin.ignore(500, '\n');
                boardSize = 0;
            }
        }
        row = boardSize;
        column = boardSize;
        while(firstPlacement < 1){
            cout<<string("Place the queen in what row? [1-") + to_string(row) << string("]")<<endl;

            char c;
            if(!(cin >> firstPlacement) || (cin.get(c) && !isspace(c))){
                qInfo()<<"Entry must be a number! Try again: ";
                cin.clear();
                cin.ignore(500, '\n');
                firstPlacement = 0;
            }
            else if(firstPlacement < 1 || firstPlacement > column){
                qInfo()<<"Number must be in Range! Try again: ";
                cin.clear();
                cin.ignore(500, '\n');
                firstPlacement = 0;
            }
        }

        firstPlacement--;
        PopulateBoard(row, column, numOfQueens, firstPlacement);
        string nextStep;
        qInfo()<<"Try again? (y or n)";
        cin>>nextStep;
        if(nextStep == "yes" || nextStep == "y"){
            boardSize = 0;
            numOfQueens = 0;
            firstPlacement = 0;
        }
        else if(nextStep == "no"|| nextStep == "n"){
            repeat = false;
        }
        else{
            boardSize = 0;
            numOfQueens = 0;
            firstPlacement = 0;
        }
    }

}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    start();
    return a.exec();
}
