#include <QCoreApplication>
#include <iostream>
#include <array>
#include "queen.h"
using namespace std;
const int N = 10;
array<array<int, N>, N> board;
//int board[N][N];
//Check if the queens position is valid
bool ValidPlacement(int rowPlace, int columnPlace, int BoardSize){
    for (int i = columnPlace; i >= 0; i--)
    {
        if(board[rowPlace][i] == 1){
            return false;
        }
    }
    for (int i = rowPlace, j = columnPlace; i >= 0 && j >= 0; i--, j--)
    {
        if(board[i][j] == 1){
            return false;
        }
    }
    for (int i = rowPlace, j = columnPlace; i >= 0 && j < BoardSize; i--, j++)
    {
        if(board[i][j] == 1){
            return false;
        }
    }
    return true;
}
//Main function to place out queens on the board
bool BackTracking(int row, int col, int numQueens, int boardSize){
    if (col == numQueens){
        return true;
    }
    for (int i = 0; i < boardSize; i++){
        int newRow = (row + i) % boardSize;
        if(ValidPlacement(newRow, col, boardSize)){
            board[newRow][col] = 1;
            if(BackTracking(newRow, col + 1, numQueens, boardSize) == true){
                return true;
            }
            board[newRow][col] = 0;
        }
    }
    return false;
}
//Draw the board for the console(Could not find a way to replace cout)
void DrawBoard(int row, int column){
    for (int i = 0; i < row; i++){
        for (int i = 0; i < column; i++)
        {
            cout<<"---";
        }
        cout<<endl;
        for (int j = 0; j < column; j++){
            cout<<"|";
            if(board[i][j]==1){
                cout<<"X";
            }
            else{
                cout<<" ";
            }
            cout<<"|";
        }
        cout<<endl;
    }
    for (int i = 0; i < column; i++)
    {
        cout<<"---";
    }
    cout<<endl;
}
//Function to fill the board
void PopulateBoard(int row, int column, int numOfQueens, int startRow){
    //Fill board with zeroes
    bool boardPassed = false;
    for (int i = 0; i < row; i++){
        for (int j = 0; j < column; j++)
        {
            board[i][j] = 0;
        }
    }
    //Solution will not work if there is more queens than board size.
    if(numOfQueens > row){
        qInfo()<<"Too many queens or too small board";
    }
    else if(BackTracking(startRow, 0, numOfQueens, row) == false){
        qInfo()<<"Solution does not work";
    }
    else{
        boardPassed = true;
        DrawBoard(row, column);
    }
    Q_ASSERT(boardPassed);
}
