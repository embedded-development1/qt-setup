#ifndef QUEEN_H
#define QUEEN_H
#pragma once

void PopulateBoard(int row, int column, int numOfQueens, int startRow);
bool BackTracking(int row, int col, int numQueens, int boardSize);
void DrawBoard(int row, int column);
bool ValidPlacement(int rowPlace, int columnPlace, int BoardSize);

#endif // QUEEN_H
