#ifndef MYNOTEPAD_H
#define MYNOTEPAD_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MyNotepad; }
QT_END_NAMESPACE

class MyNotepad : public QMainWindow
{
    Q_OBJECT

public:
    MyNotepad(QWidget *parent = nullptr);
    ~MyNotepad();

    void loadFile(const QString &filename);

protected:
    void closeEvent(QCloseEvent *event) override;

private slots:
    void on_plainTextEdit_copyAvailable(bool b);

    void on_actionAbout_Qt_triggered();

    void on_action_About_triggered();

    void on_action_New_triggered();

    void on_action_Open_triggered();

    bool on_action_Save_triggered();

    bool on_actionSave_As_triggered();

private:
    Ui::MyNotepad *ui;
    QString curFile;

    void setCurrentFile(const QString &filename);
    bool saveFile(const QString &filename);
    bool areYouSure();
    void writeSettings();
    void readSettings();
};
#endif // MYNOTEPAD_H
