#include "mynotepad.h"
#include "ui_mynotepad.h"
#include <QtWidgets>

MyNotepad::MyNotepad(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MyNotepad)
{
    ui->setupUi(this);

    ui->action_New->setShortcut(QKeySequence::New);
    ui->action_Open->setShortcut(QKeySequence::Open);
    ui->action_Save->setShortcut(QKeySequence::Save);
    ui->actionSave_As->setShortcut(QKeySequence::SaveAs);
    ui->actionE_xit->setShortcut(QKeySequence::Quit);
    ui->actionCu_t->setShortcut(QKeySequence::Cut);
    ui->action_Copy->setShortcut(QKeySequence::Copy);
    ui->action_Paste->setShortcut(QKeySequence::Paste);

    readSettings();
    setCurrentFile(QString());
    connect(ui->plainTextEdit->document(),
            &QTextDocument::contentsChanged,
            this,
            [this](){
        setWindowModified(
                    ui->plainTextEdit->document()->isModified());
    });
    statusBar()->showMessage(tr("Ready"));
}

MyNotepad::~MyNotepad()
{
    delete ui;
}

void MyNotepad::loadFile(const QString &filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                tr("Cannot read file %1:\n%2.")
                .arg(QDir::toNativeSeparators(filename),
                     file.errorString()));
        return;
    }

    QTextStream in(&file);
    QGuiApplication::setOverrideCursor(Qt::WaitCursor);
    ui->plainTextEdit->setPlainText(in.readAll());
    QGuiApplication::restoreOverrideCursor();

    setCurrentFile(filename);
    statusBar()->showMessage(tr("File loaded"), 2000);
}

void MyNotepad::closeEvent(QCloseEvent *event)
{
    if(areYouSure()){
        writeSettings();
        event->accept();
    }
    else{
        event->ignore();
    }
}


void MyNotepad::on_plainTextEdit_copyAvailable(bool b)
{
    ui->actionCu_t->setEnabled(b);
    ui->action_Copy->setEnabled(b);
}

void MyNotepad::setCurrentFile(const QString &filename)
{
    curFile = filename;
    ui->plainTextEdit->document()->setModified(false);
    setWindowModified(false);

    QString shownName = curFile;
    if(curFile.isEmpty()){
        shownName = "Untitled.txt";
    }
    setWindowFilePath(shownName);
}

bool MyNotepad::saveFile(const QString &filename)
{
    QString errorMessage;

    QGuiApplication::setOverrideCursor(Qt::WaitCursor);
    QSaveFile file(filename);
    if(file.open(QFile::WriteOnly | QFile::Text)){
        QTextStream out(&file);
        out << ui->plainTextEdit->toPlainText();
        if(!file.commit()){
            errorMessage = tr("Cannot write file %1:\n%2.")
                    .arg(QDir::toNativeSeparators(filename), file.errorString());
        }
    } else {
        errorMessage = tr("Cannot open file %1 for writing:\n%2.")
                .arg(QDir::toNativeSeparators(filename), file.errorString());
    }
    QGuiApplication::restoreOverrideCursor();

    if(!errorMessage.isEmpty()){
        QMessageBox::warning(this, tr("Application"), errorMessage);
        return false;
    }

    setCurrentFile(filename);
    statusBar()->showMessage(tr("File saved"), 2000);
    return true;
}

bool MyNotepad::areYouSure()
{
    if(!ui->plainTextEdit->document()->isModified())
        return true;
    const QMessageBox::StandardButton ret
            = QMessageBox::warning(this, tr("Application"),
                                   tr("The document has been modified.\n"
                                      "Do you want to save your changes?"),
                                   QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    switch(ret){
    case QMessageBox::Save:
        return on_action_Save_triggered();
    case QMessageBox::Cancel:
        return false;
    default:
        break;
    }
    return true;
}

void MyNotepad::writeSettings()
{
    QSettings settings(QCoreApplication::organizationName(),
                       QCoreApplication::applicationName());
    settings.setValue("geometry", saveGeometry());
}

void MyNotepad::readSettings()
{
    QSettings settings(QCoreApplication::organizationName(),
                       QCoreApplication::applicationName());
    const QByteArray geometry = settings.value("geometry", QByteArray()).toByteArray();

    if(geometry.isEmpty()){
        const QRect availableGeometry = screen()->availableGeometry();
        resize(availableGeometry.width() / 3, availableGeometry.height() / 2);
        move((availableGeometry.width() - width()) / 2,
             (availableGeometry.height() - height()) / 2);
    } else{
        restoreGeometry(geometry);
    }
}


void MyNotepad::on_actionAbout_Qt_triggered()
{
    qApp->aboutQt();
}


void MyNotepad::on_action_About_triggered()
{
    QMessageBox::about(this, tr("About MyNotePad"),
                       tr("The <b>MyNotepad</b> example demonstrates how to "
                          "write modern GUI apllications using Qt, with a menu bar,"
                          "toolbars, and a status bar."));
}


void MyNotepad::on_action_New_triggered()
{
    if(areYouSure()){
        ui->plainTextEdit->clear();
        setCurrentFile(QString());
    }
}


void MyNotepad::on_action_Open_triggered()
{
    if(areYouSure()){
        QString filename =
                QFileDialog::getOpenFileName(this);
        if(!filename.isEmpty())
            loadFile(filename);
    }
}


bool MyNotepad::on_action_Save_triggered()
{
    if(curFile.isEmpty()){
        return on_actionSave_As_triggered();
    } else{
        return saveFile(curFile);
    }
}


bool MyNotepad::on_actionSave_As_triggered()
{
    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    if(dialog.exec() != QDialog::Accepted)
        return false;
    return saveFile(dialog.selectedFiles().first());
}

