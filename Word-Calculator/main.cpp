#include <QCoreApplication>
#include <iostream>
#include <vector>
#include <limits>
#include "calculator.h"
using namespace std;

bool running = true;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QTextStream qin(stdin);
    QString test;
    bool passed = false;
    while(running){
        test = "";
        qInfo()<< "Welcome to word calculator!";
        qInfo() << "Type numbers and modifier in words";
        qInfo() << "+ = plus";
        qInfo() << "- = minus";
        qInfo() << "* = times";
        qInfo() << "/ = over";
        qInfo()<<"Write a sentence: ";
        test = qin.readLine();
        string convert = test.toStdString();
        vector<string> wordList = ProcessWord(convert);
        if(ValidWords(wordList)){
            double final = InterperetWords(wordList);
            qInfo()<<final;
            passed = true;
            Q_ASSERT(passed);
            qInfo()<<"Try Again?(y or n)";
        }
        else{
            qInfo()<<"Try Again?(y or n)";
        }
        string x;
        cin >> x;
        while(x != "y" && x != "n"){
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            qInfo() << "Please type y or n!";
            cin >> x;
        }
        if(x == "n"){
            running = false;
        }
        else{
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
    }

    return a.exec();
}
